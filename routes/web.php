<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AddController;
use App\Http\Controllers\showcontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';





Route::view('add','insert');
Route::post('add',[AddController::class, 'inserter']);
Route::get('',[showcontroller::class, 'index']);

Route::get('',[showcontroller::class, 'shower']);
Route::get('list/{ID}',[showcontroller::class, 'shower']);
Route::get('list/{ID}',[showcontroller::class, 'show']);
Route::get('show/{ID}',[showcontroller::class, 'show']);

Route::view('hades','hades');
Route::view('forzahorizon','forzahorizon');
Route::view('newworld','newworld');
Route::view('tombraider','tombraider');